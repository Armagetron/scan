# scan #

This is a small program to compute scan operations [1]. I used this to verify my examples.

### How do I get set up? ###

The software comes with a CMake build file. You can run:
```
mkdir build && cd build
cmake ..
make
./scan --log_level=message
```

### Developer ###

* Alexander Kammeyer [mail](mailto://a.kammeyer@fu-berlin.de) / [telegram](https://telegram.me/kammeyer)

### Sources ###
[1] Prefix Sums and Their Applications, Guy E. Blelloch, 1990, School of Computer Science, Carnegie Mellon University