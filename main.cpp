#define BOOST_TEST_MODULE scan_test
#include <boost/test/included/unit_test.hpp>
#include <vector>
#include <iostream>
#include <string>

struct F {
    F() { BOOST_TEST_MESSAGE( "setup fixture" ); }
    ~F() { BOOST_TEST_MESSAGE( "teardown fixture" ); }
};

template<class T>
void print_vec(std::vector<T> &v) {
    std::stringstream ss;
    for(T i : v) {
        ss << i << " ";
    }
    ss << std::endl;
    BOOST_TEST_MESSAGE(ss.str());
}

template<class T>
void scan(std::vector<T> &v)
{
    T sum = 0;
    for(auto i = 0; i < v.size(); i++) {
        T val = v[i];
        v[i] = sum;
        sum += val;
    }
}

BOOST_FIXTURE_TEST_SUITE(random_test, F)

    BOOST_AUTO_TEST_CASE(iter1)
    {
        BOOST_TEST_MESSAGE("running iter1");

        std::vector<unsigned int> v {0, 0, 1, 3, 0, 3, 0, 0, 1, 2};
        scan(v);

        for(auto i = 1; i < v.size(); i++) {
            BOOST_TEST(v[i - 1] <= v[i]);
        }

        print_vec(v);
    }

    BOOST_AUTO_TEST_CASE(iter2)
    {
        BOOST_TEST_MESSAGE("running iter2");

        std::vector<unsigned int> v {3, 2, 0, 0, 1, 2, 0, 1, 1, 0};
        scan(v);

        for(auto i = 1; i < v.size(); i++) {
            BOOST_TEST(v[i - 1] <= v[i]);
        }

        print_vec(v);
    }

    BOOST_AUTO_TEST_CASE(iter3)
    {
        BOOST_TEST_MESSAGE("running iter3");

        std::vector<unsigned int> v {6, 0, 0, 0, 1, 0, 1, 1, 1, 0};
        scan(v);

        for(auto i = 1; i < v.size(); i++) {
            BOOST_TEST(v[i - 1] <= v[i]);
        }

        print_vec(v);
    }

BOOST_AUTO_TEST_SUITE_END() //scan_test